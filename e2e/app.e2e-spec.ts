import { BambooPocPage } from './app.po';

describe('bamboo-poc App', () => {
  let page: BambooPocPage;

  beforeEach(() => {
    page = new BambooPocPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
